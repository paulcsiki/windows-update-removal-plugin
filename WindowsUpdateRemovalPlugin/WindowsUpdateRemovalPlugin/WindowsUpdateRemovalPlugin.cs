﻿#region License

// Copyright (C) 2015 Alexandru Paul Csiki
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// 
// Linking this library statically or dynamically with other modules is
// making a combined work based on this library.  Thus, the terms and
// conditions of the GNU General Public License cover the whole
// combination.

#endregion

using System;
using System.Diagnostics;
using MM.Monitor.Client;
using WUApiLib;
using Version = MM.Monitor.Client.Version;

namespace PaulCsiki.WindowsUpdateRemovalPlugin
{
    public class WindowsUpdateRemovalPlugin : ClientPlugin
    {
        private const int PAGE_ID = 1;
        private const string KB_FIELD_ID = "KB";
        private const int SEARCH_UPDATE_ID = 1;
        private const int REMOVE_UPDATE_ID = 2;
        private const int INVALID_KB_NUMBER = -1;
        private IUpdate foundUpdate;
        private int kbNumber = INVALID_KB_NUMBER;
        private AutomaticUpdates updates;
        private bool searchingForUpdate;

        public override string GetPluginName()
        {
            return "Windows Update Removal";
        }

        public override Version GetPluginVersion()
        {
            FileVersionInfo ver = FileVersionInfo.GetVersionInfo(typeof (WindowsUpdateRemovalPlugin).Assembly.Location);
            return new Version(ver.FileMajorPart, ver.FileMinorPart);
        }

        public override Version GetMinimumRequiredAgentVersion()
        {
            return new Version(4, 8, 4);
        }

        public override string GetPluginDescription()
        {
            return "Allows you to uninstall Windows Updates.";
        }

        public override void PluginLoaded()
        {
            updates = new AutomaticUpdatesClass();
            updates.EnableService();
        }

        public override Groups GetAdditionalComputerDetails()
        {
            return new Groups
            {
                new Group("Windows Update Removal")
                {
                    Items =
                    {
                        new PageItem(PAGE_ID, "Open Plugin")
                    }
                }
            };
        }

        public override Groups GetPageDetails(int pageId)
        {
            if (pageId != PAGE_ID)
            {
                Trace("Invalid page id: " + pageId);
                return new Groups {new Group("Error") {Items = {new SimpleItem("Invalid page id.", pageId.ToString(), SimpleItemStyle.ERROR)}}};
            }

            Groups container = new Groups();
            Group controls = new Group("Find update");
            controls.Items.Add(new NumberInputItem(KB_FIELD_ID, "KB Number", kbNumber < 0 ? "Not set" : kbNumber.ToString(), kbNumber < 0 ? 1 : kbNumber));
            if (searchingForUpdate)
            {
                controls.Items.Add(new SimpleItem("Searching for update..."));
            }
            else
            {
                controls.Items.Add(new CommandItem(SEARCH_UPDATE_ID, "Search"));
            }
            container.Add(controls);

            Group foundUpdatesGroup = new Group("Found Update");
            if (!searchingForUpdate && foundUpdate != null)
            {
                string id = REMOVE_UPDATE_ID + kbNumber + "";
                foundUpdatesGroup.Items.Add(new CommandItem(int.Parse(id), foundUpdate.Title, foundUpdate.Description));
                container.Add(foundUpdatesGroup);
            }
            else if (kbNumber != INVALID_KB_NUMBER && !searchingForUpdate)
            {
                foundUpdatesGroup.Items.Add(new SimpleItem("Update not found.", kbNumber.ToString(), SimpleItemStyle.WARNING));
                container.Add(foundUpdatesGroup);
            }

            return container;
        }

        public override void PageCommandReceived(int pageId, int commandId)
        {
            if (pageId != PAGE_ID)
            {
                Trace("Invalid page id: " + pageId);
                return;
            }
            string stringId = commandId.ToString();
            if (commandId == SEARCH_UPDATE_ID)
            {
                if (kbNumber < 0)
                {
                    Trace("Cannot search for updates since KB number is not set.");
                    return;
                }
                if (searchingForUpdate) return;
                SearchForUpdates();
            }
            else if (stringId.StartsWith(REMOVE_UPDATE_ID.ToString()))
            {
                int foundKb = int.Parse(stringId.Substring(1));
                RemoveUpdate(foundKb);
            }
            else
            {
                Trace("Invalid page command id: " + commandId);
            }
        }
        public override void NumericInputValueChanged(string inputId, int inputValue)
        {
            if (inputId != KB_FIELD_ID)
            {
                Trace("Got an invalid input id: " + inputId);
                return;
            }

            kbNumber = inputValue;
            SearchForUpdates();
        }

        private void SearchForUpdates()
        {
            foundUpdate = null;
            searchingForUpdate = true;
            try
            {
                var session = new UpdateSessionClass();
                var searcher = session.CreateUpdateSearcher();
                searcher.Online = false;
                var result = searcher.Search("IsInstalled=1 and Type='Software'");
                string targetKb = kbNumber.ToString();
                foreach (IUpdate update in result.Updates)
                {
                    if (string.IsNullOrWhiteSpace(update.Title) || !update.Title.Contains(targetKb)) continue;
                    foundUpdate = update;
                    break;
                }
            }
            catch (Exception ex)
            {
                Trace(ex.ToString());
            }
            finally
            {
                searchingForUpdate = false;
            }
        }
        private void RemoveUpdate(int kb)
        {
            kbNumber = INVALID_KB_NUMBER;
            ProcessStartInfo si = new ProcessStartInfo("wusa")
            {
                CreateNoWindow = true,
                Arguments = "/uninstall /quiet /norestart /kb:" + kb,
                UseShellExecute = true,
                WindowStyle = ProcessWindowStyle.Hidden
            };
            Process.Start(si);
        }
    }
}