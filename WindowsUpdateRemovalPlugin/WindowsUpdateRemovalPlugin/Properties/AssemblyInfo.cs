﻿#region License

// Copyright (C) 2015 Alexandru Paul Csiki
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// 
// Linking this library statically or dynamically with other modules is
// making a combined work based on this library.  Thus, the terms and
// conditions of the GNU General Public License cover the whole
// combination.

#endregion

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Windows Update Removal Plugin")]
[assembly: AssemblyDescription("A Pulseway plugin that allows you to uninstall Windows Updates")]
[assembly: AssemblyCompany("Alexandru Paul Csiki")]
[assembly: AssemblyProduct("Windows Update Removal Plugin")]
[assembly: AssemblyCopyright("Copyright © Alexandru Paul Csiki 2015")]
[assembly: ComVisible(false)]
[assembly: Guid("bfc78393-f94d-4d7e-9b87-b003024c455a")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]